# Study Path

## Onboarding

### Basics
* Read **The Pragmatic Programmer**:
  * Chapter 1: A Pragmatic Philosophy
  * Chapter 2: A Pragmatic Approach


* Read **Clean Code**:
  * Chapter 1: Clean Code
  * Chapter 2: Meaningful Names
  * Chapter 3: Functions
  * Chapter 4: Comments
  * Chapter 6: Objects and Data Structure
  * Chapter 7: Error Handling
  * Chapter 8: Boundaries
  * Chapter 9: Unit Tests
  * Chapter 10: Classes


* [**HANDS ON**] Do (also if you already did) the [String Calculator Kata](https://gitlab.com/cherry-chain/string-calculator-kata)
  * Apply the concepts you learned

* [**HANDS ON**] Another exercise you can do is [Tennis Kata](https://gitlab.com/cherry-chain/tennis-kata)

### Values, Principles and Practices

* Read **Extreme Programming Explained**:
  * Chapter 1: What is XP?
  * Section 1: Exploring XP


* Read **Test-Driven Development: by example**:
  * Part I: The Money Example


* [**HANDS ON**] Do "The money example" by yourself


* Watch [The Clean Code Talks - Unit Testing](https://www.youtube.com/watch?v=wEhu57pih5w) (~ 30 minutes)
* Watch [The Clean Code Talks - Inheritance, Polymorphism, & Testing](https://www.youtube.com/watch?v=4F72VULWFvc) (~ 40 minutes)


* [**HANDS ON**] Do the [Sales Kata](https://github.com/xpeppers/sales-taxes-problem) in TDD


* Read **Refactoring - Improving the Design of Existing Code**:
  * Chapter 1: Refactoring, a First Example
  * Chapter 2: Principles in Refactoring
  * Chapter 3: Bad Smells in Code


* [**HANDS ON**] Do the [Gilded Rose Kata](https://github.com/emilybache/GildedRose-Refactoring-Kata)
  * Cover code with tests
  * Refactor Code
  * Add the new feature


* Read [The Four Elements of Simple Design](https://blog.jbrains.ca/permalink/the-four-elements-of-simple-design)

* Learn the  [**SOLID Principles**](http://butunclebob.com/ArticleS.UncleBob.PrinciplesOfOod):
  * Chapter 7: SRP - The Single Responsability Principle
  * Chapter 8: OCP - The Open-Closed Principle
  * Chapter 9: LSP - The Liskov Substitution Principle
  * Chapter 10: ISP - The Interface Segregation Principle
  * Chapter 11: DIP - The Dependency Inversion Principle


* [**HANDS ON**] Do the [Racing Car Katas](https://github.com/emilybache/Racing-Car-Katas)
  * find SOLID violations

## Keep on Learning

### Agile methodology
* Watch: [7 minutes, 26 seconds and the Fundamental Theorem of Agile Software Development](https://www.youtube.com/watch?v=WSes_PexXcA)
* Read: [No Silver Bullet](http://worrydream.com/refs/Brooks-NoSilverBullet.pdf)
### TDD and testing
* Read **Test-Driven Development: by example**:
  * Part II: The xUnit Example
  * Part III: Patterns for Test-Driven Development
* Watch [The Clean Code Talks - Don't Look For Things!](https://www.youtube.com/watch?v=RlfLCWKxHJ0) (~ 35 minutes)
* Watch [The Clean Code Talks - "Global State and Singletons](https://www.youtube.com/watch?v=-FRm3VPhseI) (~55 minutes)
* Read [Mocks Aren't Stubs](https://martinfowler.com/articles/mocksArentStubs.html)
* Watch [How to Write Clean, Testable Code](https://www.youtube.com/watch?v=XcT4yYu_TTs)
* Read [Integrated Tests Are a Scam](https://blog.thecodewhisperer.com/permalink/integrated-tests-are-a-scam)

### Architecture
* Hexagonal Architecture
* Clean Architecture (book)
* [Design Patterns](https://github.com/iluwatar/java-design-patterns)

### CQRS and Event Sourcing
* Watch [Greg Young - CQRS and Event Sourcing](https://www.youtube.com/watch?v=JHGkaShoyNs)
* [Exploring CQRS and Event Sourcing](https://www.microsoft.com/en-us/download/details.aspx?id=34774)
* [CQRS The Example](https://leanpub.com/cqrs)
* [Axon Framework](https://axoniq.io/)  

## Exercises

* [Coding Dojo Katas](http://codingdojo.org/kata/)
* [Emily Bache Katas](http://coding-is-like-cooking.info/category/code-kata/)

## Quick Links
* [Agile Manifesto](https://agilemanifesto.org/)
* [Principles behind the Agile Manifesto](http://agilemanifesto.org/principles.html)
* [Manifesto for Sofware Craftmanship](http://manifesto.softwarecraftsmanship.org/)
* [eXtreme Programming values](http://www.extremeprogramming.org/values.html)
