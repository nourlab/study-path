# Links

## Api documentation
* https://docs.pact.io/
### Vertx
* https://github.com/ckaratzas/vertx-openapi-spec-generator
* https://github.com/outofcoffee/vertx-oas
## Async schema registry
* https://github.com/Yelp/schematizer
* https://engineeringblog.yelp.com/2016/08/more-than-just-a-schema-store.html

## Coding
* https://www.oreilly.com/programming/free/object-oriented-vs-functional-programming.csp

## DLT
* https://www.ethereum.org/dao

## Kafka
* https://blog.cloudera.com/blog/2018/07/robust-message-serialization-in-apache-kafka-using-apache-avro-part-1/
* https://www.confluent.io/kafka-summit-london18/distributed-data-quality-technical-solutions-for-organizational-scaling
* https://rmoff.net/2018/08/02/kafka-listeners-explained/

## Microservices
* https://www.slideshare.net/StefanoRocco/evolutionary-systems-kafka-microservices?qid=4cdd0456-e931-4403-9924-7529f5cb8ff0&v=&b=&from_search=1

## Testing
* https://opensource.googleblog.com/2018/01/container-structure-tests-unit-tests.html