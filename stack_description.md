# Descrizione stack tecnologico CherryChain

## Versionamento e deploying

In questa sezione parleremo di come teniamo traccia delle modifiche al codice sorgente e del processo di deployment.

### Gitlab
Lo strumento di riferimento per il versionamento del codice è `git`, mentre il repository attualmente in uso è [Gitlab](https://www.gitlab.com/).

All'interno del repository Gitlab è stato creato un singolo gruppo di lavoro: CherryChain. In questo gruppo di lavoro sono contenuti tutti i progetti finora sviluppati.

### Gitlab CI
Lo strumento di riferimento per l'integrazione continua è Gitlab CI.

Gitlab CI prevede la configurazione di task runner all'interno di ogni singolo progetto, tramite il file `.gitlab-ci.yml`. Ogni task runner si occupa di avviare i test del progetto e costruire un immagine docker a partire dalla build del progetto. L'immagine docker viene caricata infine nel docker registry di Gitlab.

### Docker registry
Lo strumento di riferimento per la registrazione di immagini docker è Gitlab docker registry.

---

## Monitoring
TODO

---

## Logging

In questa sezione parleremo di come teniamo traccia dei log raccolti dalle applicazioni.

### Logstash

Lo strumento di riferimento per il parsing dei logs è Logstash.

Per raccogliere logs prodotti da una applicazione viene configurato un servizio chiamato Filebeat. Il servizio Filebeat è installato nell'immagine docker centos7, ed è un watcher che monitora ed invia i logs alla istanza Logstash remota.

Logstash si occupa di effettuare il parsing dei logs tramite la configurazione Grock e l'inoltro all'istanza di Elasticsearch.

### Elasticsearch
Lo strumento di riferimento di archiviazione e ricerca dei logs è Elasticsearch.

Elasticsearch è un servizio di ricerca con capacità "Full Text". È dove logstash inoltra i logs una volta che li ha parsati.

### Kibana
Lo strumento di riferimento per la consultazione dei logs è Kibana.

Kibana è un pannello di amministrazione con interfaccia web per Elasticsearch e permette di consultare i logs raccolti.

---

## Provisionig
TODO

---

## Load Balancing
TODO

---

## Virtualizzazione e orchestrazione
In questa serzione parleremo di come vengono virtualizzati e orchestrati i vari servizi.

### Docker
Lo strumento di riferimento per la virtualizzazione è Docker.

Docker è uno strumento che può pacchettizzare un'applicazione e le sue dipendenze in un container virtuale che può essere eseguito su qualsiasi server Linux.

---

## API Gateway e service meshing
TODO

---

## Security
TODO

---

## Front servicing
In questa sezione parleremo di come è ingegnerizzato il front-end applicativo.

### Vue.js
Lo strumento di riferimento per il front servicing è Vue.js

Vue.js e' un framework Javascript per creare interfacce utente e single-page applications.

---

## Back servicing
### Eclipse Vert.x
Il framework di riferimento per i servizi back-end è [Eclipse Vert.x](https://vertx.io/).

Vert.x è uno strumento per lo sviluppo di tipo reattivo: più propriamente detto ad eventi con loop non bloccante.

---

## Messaging e computing

### Apache Kafka e Zookeeper
Lo strumento di riferimento per il messaging è Apache Kafka.

La comunicazione tra più applicazioni avviene tramite Kafka, piattaforma di stream processing e messaging.

Il servizio Kafka è gestito a sua volta da Apache Zookeeper, un sistema di sincronizzazione per sistemi distribuiti.

---

## Storing
In questa sezione parleremo del meccanismo utilizzato per conservare i dati utilizzati dai vari servizi.

### PostgreSQL e PipelineDB
Lo strumento di riferimento per RDBMS è PostgreSQL.

Se il servizio dovesse avere necessita di fare streaming di dati PipelineDB è una estensione per PostgreSQL che rende possibili operazioni di streaming di dati all'interno di un database relazionale.

---

## Analytics
TODO

---

## Integrating
TODO
