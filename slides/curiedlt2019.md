# Distributed Ledgers Technology

![bg](cherry.jpg)

---

# Il Ledger

E' il "Libro Mastro", ovvero la base fondamentale della contabilità.

I Ledger sono stati il grande centro di attenzione dell’informatizzazione nelle grandi aziende e nelle Pubbliche Amministrazioni.

![bg original](bg.png)

---

# Logica centralizzata

Per molto tempo i Ledger sono stati interpretati all'interno di un ottica centralizzata, come avviene per i database.

![bg original](bg.png)

---

![](images/centralized.png)

![bg original](bg.png)

---

# Logica distribuita

I DLT sono dei  Database Distribuiti o Registri Distribuiti, ovvero dei Ledgers che possono essere aggiornati, gestiti, controllati e coordinati in modo distribuito, da tutti gli attori che interagiscono e governano il sistema.

![bg original](bg.png)

---

# Reti DLT

I presupposti per i Distributed Ledgers Technology sono nella creazione di reti costituiti da una serie di partecipanti e ogni partecipante è chiamato a gestire uno o piu' nodi di questa rete.

Ciascun nodo è autorizzato ad aggiornare i Distributed Ledgers in modo indipendente dagli altri ma sotto il controllo consensuale degli altri nodi.

![bg original](bg.png)

---

# Consenso distribuito

Gli aggiornamenti o records sono creati e caricati da ciascun nodo in modo indipendente.

In questo modo ogni partecipante è in grado di processare e controllare ogni transazione che deve essere verificata, votata e approvata dalla maggioranza dei partecipanti alla rete.

L’autonomia di ciascun nodo è subordinata al raggiungimento di un consenso sulle operazioni che vengono svolte e solo con questo consenso sono poi autorizzate e attivate.

![bg original](bg.png)

---

![](images/distributed.png)

![bg original](bg.png)

---

# Blockchain

Una blockchain e' una struttura dati immutabile utilizzata per implementare DLT. Fondamentalmente e' una catena di blocchi.

<br>

![](images/block.png)

![bg original](bg.png)

---

# Blockchain

I blocchi sono legati tra di loro tramite un codice di controllo.

![](images/bitcoin_block_data.png)

![bg original](bg.png)

---

# Unpermissioned ledgers

Le Unpermissioned Ledgers di cui l’esempio più famoso e diffuso è rappresentato dalla Blockchain Bitcoin, sono aperte, non hanno una “proprietà” o un attore di riferimento e sono concepite per non essere controllate.

L’obiettivo delle Unpermissioned ledgers è quello di permettere a ciascuno di contribuire all’aggiornamento dei dati sul Ledger e di disporre, in qualità di partecipante, di tutte le copie immutabili di tutte le operazioni

![bg original](bg.png)

---

# Permissioned ledgers

Questo tipo di Blockchain possono essere utilizzate da istituzioni, grandi imprese che devono gestire filiere con una serie di attori, imprese che devono gestire fornitori e subfornitori, banche, società di servizi, operatori nell’ambito del retail.

Le Permissioned ledgers permettono di definire speciali regole per l’accesso e la visibilità dei dati. Introducono nella Blockchain un concetto di Governance e di definizione di regole di comportamento.

![bg original](bg.png)

---

# Smart Contract

Lo smart contract è "un programma per elaboratore che opera su Tecnologie basate su registri distribuiti (...) la cui esecuzione vincola automaticamente due o più parti sulla base di effetti predefiniti dalle stesse"

![bg original](bg.png)

---

![](images/sc_insurance_policies.png)

![bg original](bg.png)

---

# Riferimenti

![bg original](bg.png)

---

- https://www.blockchain4innovation.it/esperti/cosa-funzionano-le-blockchain-distributed-ledgers-technology-dlt/#La_famiglia_delle_Distributed_Ledger_e_la_blockchain
- https://www.blockchain4innovation.it/mercati/legal/smart-contract/blockchain-smart-contracts-cosa-funzionano-quali-gli-ambiti-applicativi/
- https://www.altoros.com/blog/the-difference-with-blockchain/
- https://en.wikipedia.org/wiki/Blockchain

![bg original](bg.png)

---
