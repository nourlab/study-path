# The Pragmatic Programmer

![bg](cherry.jpg)

---

# A Pragmatic Approach

![bg](cherry.jpg)

---

# Orthogonality

“Orthogonality” is a term borrowed from geometry.

In computing, the term has come to signify a kind of independence or decoupling. Two or more things are orthogonal if changes in one do not affect any of the others. In a well-designed system, the database code will be orthogonal to the user interface: you can change the interface without affecting the database, and swap databases without changing the interface.

![bg](cherry.jpg)

---

# Reversibility

Instead of carving decisions in stone, think of them more as being written in the sand at the beach. A big wave can come along and wipe them out at any time.

![bg](cherry.jpg)

---

# Domain Specific Languages

We always try to write code using the vocabulary of the application domain. In some cases, we can go to the next level and actually program using the vocabulary, syntax, and semantics of the domain.

![bg](cherry.jpg)

---

![](images/dsl.jpg)

How can one vizualise a DSL? It’s not easy, but let’s just say a DSL is a clean, shiny layer on top of low-level constructs.

---

# Estimating

By learning to estimate, and by developing this skill to the point where you have an intuitive feel for the magnitudes of things, you will be able to show an apparent magical ability to determine their feasibility. When someone says "we'll send the backup over an ISDN line to the central site" you’ll be able to know intuitively whether this is practical. When you’re coding, you’ll be able to know which subsystems need optimizing and which ones can be left alone.

![bg](cherry.jpg)

---

# Bend or Break

![bg](cherry.jpg)

---

# Metaprogramming

Details mess up our pristine code, especially if they change frequently. Every time we have to go in and change the code to accommodate some change in business logic, or in the law, or in personal tastes, we run the risk of breaking the system.

So we say "out with the details!" Get them out of the code. While we're at it, we can make our code highly configurable and "soft", that is, easily adaptable to changes.

![bg](cherry.jpg)

---

# Temporal Coupling

We need to allow for concurrency and to think about decoupling any time or order dependencies. In doing so, we can gain flexibility and reduce any time-based dependencies in many areas of development: workflow analysis, architecture, design, and deployment.

---

![bg](cherry.jpg)

---

# While You Are Coding

![bg](cherry.jpg)

---

# Program Deliberately

- Always be aware of what you are doing.
- Attempting to build an application you don’t fully understand, or to use a technology you aren’t familiar with, is an invitation to be misled by coincidences.
- Proceed from a plan
- Rely only on reliable things. Don’t depend on accidents or assumptions.
- Document your assumptions.
- Prioritize your effort. Spend time on the important aspects; morethan likely, these are the hard parts.

![bg](cherry.jpg)

---

![bg](cherry.jpg)
