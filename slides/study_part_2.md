<!-- https://medium.com/technology-learning/event-sourcing-and-cqrs-a-look-at-kafka-e0c1b90d17d8 -->

# Topics for this seminary

- Event Sourcing
- CQRS
- Hexagonal Architecture

![bg](cherry.jpg)

---

# CRUD

In the create, read, update, and delete (CRUD) model, a typical data process will be to read data from the store, make some modifications to it, and update the current state of the data with the new values, **using transactions that lock the data**

![bg](cherry.jpg)

---

# CRUD approach: limitations

CRUD requires 2 phase commit(2PC) when working with event driven systems by using a  distributed transaction involving the database and the Message Broker.

- 2PC commit reduce the throughput of transactions.

- In a collaborative domain with many concurrent users, data update conflicts are more likely to occur because the update operations take place on a single item of data.

- Unless there is an additional auditing mechanism, which records the details of each operation in a separate log, history is lost.

![bg](cherry.jpg)

---

![](images/2pc.jpeg)

---

# Event sourcing

Event sourcing achieves atomicity without 2PC.

- The application stores a sequence of state-changing events.
- The application reconstructs an entity's current state by replaying the events.
- Since saving an event is a single operation, it is inherently atomic.

![bg](cherry.jpg)

---

![](images/eventsourcing.jpeg)

---

![](images/eventsourcing.png)

---

# Why use event sourcing?

- Audit trail: events are immutable and store the full history of the state of the system.
- Integration with other subsystems: event store can publish events to notify other interested subsystems of changes to the application's state.
- Time Travel: by storing events, you have the ability to determine the state of the system at any previous point in time.

![bg](cherry.jpg)

---

# Command and Query Responsibility Segregation

CQRS is a simple design pattern for separating concerns. Here, object's methods should be either commands or queries but not both.
- A query returns data and does not alter the state of the object;
- a command changes the state of an object but does not return any data.

The benefit is that you have a better understanding of what does, and what does not change the state in your system.

![bg](cherry.jpg)

---

# Kafka as an EventStore

![](images/kafka.png)

Kafka is a message broker or message queue

---

![](images/flow.png)

---