# SOLID Principles

![bg](cherry.jpg)

---

# SOLID Principles

- Single responsability
- Open/Close
- Liskov substitution
- Interface segregation
- Dependency Injection

![bg](cherry.jpg)

---

# Single Responsability

A class should have only a single responsibility (i.e. only changes to one part of the software's specification should be able to affect the specification of the class).

![bg](cherry.jpg)

---

This class does two things a the same time, we can improve it using the **Single Responsability** principle 

<span style="font-size:50%">

```kotlin
class Person {
  var name : String = ""
  var surname : String = ""
  var email : String = ""

  constructor(name : String, surname : String, email : String) {
    this.name = name
    this.surname = surname
    this.email = email

    if (!validateEmail()) {
      throw Exception("Provided email address is invalid")
    }
  }

  fun validateEmail() : Boolean {
    val regex = """^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$"""
      .toRegex(RegexOption.IGNORE_CASE)
    
    return regex.matches(this.email)
  }
}


```
</span>

![bg](cherry.jpg)


---

Now we have two classes with clearly separated concerns!

<span style="font-size:50%">

```kotlin
class Email {
  var address : String = ""

  constructor(address : String) {
    this.address = address
    if (!validateEmail()) {
      throw Exception("Provided email address is invalid")
    }
  }

  fun validateEmail() : Boolean {
    val regex = """^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$"""
      .toRegex(RegexOption.IGNORE_CASE)

    return regex.matches(this.address)
  }
}
```

```kotlin
class Person(val name : String, val surname : String, val email : Email)
```

</span>

![bg](cherry.jpg)


---

# Open/Close

“Open For Extension” - This means that the behavior of the module can be extended. That we can make the module behave in new and different ways as the requirements of the application change.

“Closed for Modification” - The source code of such a module is inviolate. No one is allowed to make source code changes to it.

![bg](cherry.jpg)

---

This example isn't closed for modification as we need to change it in order to extend it. In other words: it isn't open for extension.

<span style="font-size:50%">

```kotlin
interface ShapesAreaApi {
  fun addSquareArea(side: Int)
  fun addRectangleArea(width: Int, height: Int)
  fun addCircleArea(radius: Int)
  // other shapes ...

  fun totalArea()
}
```

<!-- https://johnlnelson.com/2014/07/20/open-closed-principle-in-c-solid-design-principles/ -->

</span>

![bg](cherry.jpg)


---

A solution that abides by the Open/Closed Principle:

<span style="font-size:50%">

```kotlin
interface Shape {
  fun area() : Double
}
```

```kotlin
interface ShapesAreaApi {
  fun add(shape : Shape2d)
  fun totalArea()
}
```

</span>

Higher-order functions also exhibit the same property of being open for extension, despite being closed for modification.

![bg](cherry.jpg)


---

# Liskov substitution

Objects in a program should be replaceable with instances of their subtypes without altering the correctness of that program

![bg](cherry.jpg)

---

Version one of BirdsFlyingAroundApp is a huge success. Version two adds another 12 different types of birds with ease, and is also a success

<span style="font-size:50%">

```kotlin
interface Bird {
  fun setLocation(lat : Double, lng : Double)
  fun setAltitude(altitude : Double)
  fun draw()
}
```

</span>

The developer makes a new Penguin class that inherits from the Bird class, but there is a problem

<span style="font-size:50%">

```
override fun setAltitude(altitude: Double) {
  //altitude can't be set because penguins can't fly
  //this function does nothing
}
```

</span>

![bg](cherry.jpg)

---

Solution:

<span style="font-size:50%">

```kotlin
interface Bird {
  fun setLocation(lat : Double, lng : Double)
  fun draw()
}
```

```kotlin
interface FlyingBird : Bird {
  fun setAltitude(altitude : Double)
}
```

</span>

![bg](cherry.jpg)

---

# Interface Segregation

Many client-specific interfaces are better than one general-purpose interface

![bg](cherry.jpg)

---

This interface brings too much information.

<span style="font-size:50%">

```kotlin
interface SmartDevice {
  fun print()
  fun scan()
  fun fax()
}
```

```kotlin
class AllInOnePrinter : SmartDevice {
  override fun fax() {
    // Beep booop biiiiip.
  }

  override fun scan() {
    // Scanning code.
  }

  override fun print() {
    // Printing code.
  }
}
```

```kotlin
class EconomicPrinter : SmartDevice {
  override fun fax() {
    throw NotImplementedError("Not supported by implementation")
  }

  override fun scan() {
    throw NotImplementedError("Not supported by implementation")
  }

  override fun print() {
    // Printing code.
  }
}
```
</span>

![bg](cherry.jpg)

---

<span style="font-size:50%">

```kotlin
interface Printer {
  fun print()
}
```
```kotlin
interface Scanner {
  fun scan()
}
```
```kotlin
interface Fax {
  fun fax()
}
```
```kotlin
class AllInOnePrinter : Printer, Scanner, Fax {
  override fun fax() {
    // Beep booop biiiiip.
  }

  override fun scan() {
    // Scanning code.
  }

  override fun print() {
    // Printing code.
  }
}
```
```kotlin
class EconomicPrinter : Printer {
  override fun print() {
    // Printing code.
  }
}
```

</span>

![bg](cherry.jpg)


---

# Dependency injection

One should depend upon abstractions, not concretions.

![bg](cherry.jpg)

---

`Veichle` is tighly copuled with `RaceCarEngine`, what if we have to add a different type of `Engine` later?

<span style="font-size:50%">
  
```kotlin
interface Engine {
  fun move()
}
```

```kotlin
class RaceCarEngine : Engine {
  override fun move() {
    println("Vrrrooooommm!!")
  }
}
```

```kotlin
class Veichle {
  var engine : Engine = RaceCarEngine()

  fun forward() {
    engine.move()
  }
}
```
</span>

![bg](cherry.jpg)

---

<span style="font-size:50%">
  
```kotlin
class RocketEngine : Engine {
  override fun move() {
    println("3-2-1... LIFT OFF... PPPPSSSSCHHHHOOOOOOOOOMMMMMM!!!")
  }
}
```

```kotlin
class Veichle(val engine : Engine) {
  fun forward() {
    engine.move()
  }
}
```
</span>

![bg](cherry.jpg)

---

---