# Clean Code

![bg](cherry.jpg)

---

# What is the cost of owning a complete mess?

Have you ever waded through a mess so grave that it took weeks to do what should have taken hours? Have you seen what should have been a one-line change, made instead in hundreds of different modules? These symptoms are all too common.

![bg](cherry.jpg)

---

# Leave the campground cleaner than you found it.

It's not enough to write the code well. The code has to be kept clean over time. We've all seen code rot and degrade as time passes. So we must take an active role in  reventing this degradation.

Can you imagine working on a project where the code *simply got better* as time passed? Do you believe that any other option is professional?

![bg](cherry.jpg)

---

# Meaningful names

![bg](cherry.jpg)

---

# Use Intention-Revealing Names

The name of a variable, function, or class, should answer all the big questions. It should tell you why it exists, what it does, and how it is used. If a name requires a comment, then the name does not reveal its intent.

![bg](cherry.jpg)

---

# Make Meaningfull Distinctions

It is not sufficient to add number series or noise words, even though the compiler is satisfied. If names must be different, then they should also mean something different.

Imagine that you have a `Product` class. If you have another called `ProductInfo` or `ProductData`, you have made the names different without making them mean anything different. `Info` and `Data` are indistinct noise words like `a`, `an`, and `the`.

![bg](cherry.jpg)

---

# Use Names You Can Pronounce

Make your names pronounceable.

A company I know has `genymdhms` (generation date, year, month, day, hour, minute, and second) so they walked around saying "gen why emm dee aich emm ess".

![bg](cherry.jpg)

---

# Functions

![bg](cherry.jpg)

---

# The Stepdown Rule

We want the code to read like a top-down narrative. We want every function to be followed by those at the next level of abstraction so that we can read the program, descending one level of abstraction at a time as we read down the list of functions. I call this The Stepdown Rule.

![bg](cherry.jpg)

---

# Command Query Separation

Functions should either do something or answer something, but not both. Either your function should change the state of an object, or it should return some information about that object. Doing both often leads to confusion.

![bg](cherry.jpg)

---

# Have no side effects

Your function promises to do one thing, but it also does other hidden things. Sometimes it will make unexpected changes to the variables of its own class.

Sometimes it will make them to the parameters passed into the function or to system globals. In either case they are devious and damaging mistruths that often result in strange temporal couplings and order dependencies.

![bg](cherry.jpg)

---

# Do One Thing

FUNCTIONS SHOULD DO ONE THING.
THEY SHOULD DO IT WELL.
THEY SHOULD DO IT ONLY.

![bg](cherry.jpg)

---

# Don't Repeat Yourself

Duplication may be the root of all evil in software.

Consider, for example, that all of Codd’s database normal forms serve to eliminate duplication in data. Consider also how object-oriented programming serves to concentrate code into base classes that would otherwise be redundant. 

It would appear that since the invention of the subroutine, innovations in software development have been an ongoing attempt to eliminate duplication from our source code.

![bg](cherry.jpg)

---

# Comments

![bg](cherry.jpg)

---

# Comments Do Not Make Up for Bad Code

One of the more common motivations for writing comments is bad code. We write a module and we know it is confusing and disorganized. We know it's a mess.

So we say to ourselves, "Ooh, I'd better comment that!"

![bg](cherry.jpg)

---

# Explain Yourself In Code

There are certainly times when code makes a poor vehicle for explanation. Unfortunately, many programmers have taken this to mean that code is seldom, if ever, a good means for explanation. This is patently false.

Use tests to explain how does a module work in conjunction with others.

![bg](cherry.jpg)

---

# Formatting

![bg](cherry.jpg)

---

# Team Rules

A team of developers should agree upon a single formatting style, and then every member of that team should use that style. We want the software to have a consistent style. We don’t want it to appear to have been written by a bunch of disagreeing individuals.

![bg](cherry.jpg)

---

# Objects and data structures

![bg](cherry.jpg)

---

# Data Abstraction

A class exposes interfaces that allow its users to manipulate the essence of the data, without having to know its implementation.

<span style="font-size:50%">

```java
public interface Point {
  double getX();
  double getY();
  void setCartesian(double x, double y);
  double getR();
  double getTheta();
  void setPolar(double r, double theta);
}
```

</span>

![bg](cherry.jpg)

---

# Data/Object Anti-Symmetry

Objects hide their data behind abstractions and expose functions that operate on that data.

Data structure expose their data and have no meaningful functions.

Notice the complimentary nature of the two definitions. They are virtual opposites.

![bg](cherry.jpg)

---

# The Law Of Demeter

The Law of Demeter says a module should not know about the innards of the objects it manipulates. Objects hide their data and expose operations. This means that an object should not expose its internal structure through accessors because to do so is to expose, rather than to hide, its internal structure.

Talk to friends, not to strangers.

![bg](cherry.jpg)

---

# Error handling

![bg](cherry.jpg)

---

# Use Exceptions Rather Than Return Codes

Back in the distant past there were many languages that didn't have exceptions.

It is better to throw an exception when you encounter an error. The calling code is cleaner. Its logic is not obscured by error handling.

![bg](cherry.jpg)

---

# Write Your `Try-Catch-Finally` Statement First

Try to write tests that force exceptions, and then add behavior to your handler to satisfy your tests. This will cause you to build the transaction scope of the try block first and will help you maintain the transaction nature of that scope.

![bg](cherry.jpg)

---

# Use Unchecked Exceptions (The debate is over)

The price of checked exceptions is an Open/Closed Principle violation. If you throw a checked exception from a method in your code and the catch is three levels above, you must declare that exception in the signature of each method between you and the catch.

![bg](cherry.jpg)

---

# Don't pass `null`

You should avoid passing `null` in your code whenever possible.

In most programming languages there is no good way to deal with a `null` that is passed by a caller accidentally. Because this is the case, the rational approach is to forbid passing null by default.

![bg](cherry.jpg)

---

# Don't Return `null`

When we return null , we are essentially creating work for ourselves and foisting problems upon our callers. All it takes is one missing null check to send an application spinning out of control.

![bg](cherry.jpg)

---

# Boundaries

![bg](cherry.jpg)

---

# Code at the boundaries needs clear separation and tests.

Learning the third-party code is hard. Integrating the third-party code is hard too. Doing both at the same time is doubly hard. What if we took a different approach? Instead of experimenting and trying out the new stuff in our production code, we could write some tests to explore our understanding of the third-party code.

![bg](cherry.jpg)

---

# Unit tests

![bg](cherry.jpg)

---

# The Three Laws of TDD

- You may not write production code until you have written a failing unit test.
- You may not write more of a unit test than is sufficient to fail, and not compiling is failing.
- You may not write more production code than is sufficient to pass the currently failing test.

![bg](cherry.jpg)

---

# Keeping Tests Clean

*Test code is just as important as production code*. It is not a second-class citizen. It requires thought, design, and care. It must be kept as clean as production code.

![bg](cherry.jpg)

---

# F.I.R.S.T
Clean tests follow five other rules that form the above acronym:
- Fast
- Indipendent
- Repetable
- Self-Validating
- Timely

![bg](cherry.jpg)

---

# Classes

![bg](cherry.jpg)

---

# Classes Should Be Small!

The first rule of classes is that they should be small. The second rule of classes is that they should be smaller than that.

With functions we measured size by counting physical lines. With classes we use a different measure. We count *responsibilities*.

![bg](cherry.jpg)

---

# Maintaining Cohesion Results in Many Small Classes

Classes should have a small number of instance variables. Each of the methods of a class should manipulate one or more of those variables. In general the more variables a method manipulates the more cohesive that method is to its class. A class in which each variable is used by each method is maximally cohesive.

![bg](cherry.jpg)

---

![bg](cherry.jpg)
