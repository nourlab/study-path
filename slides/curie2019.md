# CherryChain
### Una transazione tira l'altra

![bg](cherry.jpg)

---

## Chi siamo

CherryChain e' una startup collegata alle financial technologies nata alla fine dell'anno 2018.

Il nostro settore di sviluppo e' incentrato su tecnologie Distributed Ledger, smart contract e firma digitale.

![bg](cherry.jpg)

---

## Il Team

< inserire foto>

---
<!-- Togliere -->
## Dove siamo

CherryChain e' ospitata da FBK nella sede di Povo, TN, a due passi dall'universita' di informatica.

La Fondazione Bruno Kessler e' un ente di ricerca di interesse pubblico, senza fini di lucro, ai primi posti in Italia.

![bg](cherry.jpg)

---

## Cosa facciamo

CherryChain e' un partner fintech per realizzare nuovi servizi a valore aggiunto per:

- Migliorare l'esperienza di pagamento
- Ridurre i costi di compliance
- Migliorare la sicurezza nei processi di gestione del cashless

![bg](cherry.jpg)

---

![bg](cherry.jpg)

---

---